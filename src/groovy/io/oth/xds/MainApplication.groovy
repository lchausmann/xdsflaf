package io.oth.xds


/**
 * Created by lch on 17/06/16.
 */


import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@ComponentScan(["io","org.net4care.connector"])
@SpringBootApplication
@EnableConfigurationProperties
public class MainApplication {
    public static void main(String[] args) {

        println "************************************ STARTING ********************"

        def applicationContext = SpringApplication.run(MainApplication.class, args);


        String[] beanNames = applicationContext.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        for (String beanName : beanNames) {
            System.out.println(beanName);
        }


        println "************************************* ENDING ********************"

    }

}
