package io.oth.xds.routes

import groovy.xml.XmlUtil
import org.apache.camel.Endpoint
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.model.RouteDefinition
import org.apache.camel.model.dataformat.JaxbDataFormat
import org.apache.camel.spi.DataFormat
import org.net4care.xdsconnector.RepositoryConnector
import org.net4care.xdsconnector.Utilities.SubmitObjectsRequestHelper
import org.net4care.xdsconnector.service.ObjectFactory
import org.net4care.xdsconnector.service.ProvideAndRegisterDocumentSetRequestType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource
import org.springframework.stereotype.Component
import org.w3c.dom.Document

import javax.xml.bind.JAXBElement
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory

/**
 * Created by lch on 17/06/16.
 */
@Component
@Configuration
@EnableConfigurationProperties
@PropertySource("classpath:xds.properties")
class FileRouter extends RouteBuilder {

    @Value('${xds.repositoryUrl}')
    private String repositoryUrl;
    @Value('${xds.repositoryId}')
    private String repositoryId;
    @Value('${xds.homeCommunityId}')
    private String homeCommunityId;

    @Autowired
    private RepositoryConnector xdsRepositoryConnector

    Document getDocumentFromString(String xml) {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new ByteArrayInputStream(xml.getBytes()));
    }


       void configure() {

//           RouteDefinition e = from("file://input/?noop=true").to("log:io.oth.xds?level=INFO")
//           DataFormat jaxbDataFormat = new JaxbDataFormat("urn:hl7-org:v3")

           log.info("Starting route")


//           from("file://input/?noop=true").unmarshal(jaxbDataFormat).to("log:io.oth.xds?level=INFO")
           from("file://input/?noop=true").to("log:input").unmarshal().gpath(true)
           .process(new Processor() {
               @Override
               void process(Exchange exchange) throws Exception {
                   def request = procesCda(exchange.getIn().getBody())

//                   exchange.getOut().setBody(request)
               }
           })
//                   .to("cxf:bean:cxfEndpoint?wsdlURL=${repositoryUrl}")
           // Validate against the schema
//                   .verify().xsd().ccda_schema()
           // Validate more closely using Schematron rules
//                   .verify().schematron().ccda().to("log:io.oth.xds?level=INFO")
////
////                   process(new Processor() {
////               public void process(Exchange exchange) throws Exception {
////                   Object body = exchange.getIn().getBody();
////                   println "Got: \n" + body
////                   // do some business logic with the input body
////               }
//           })
            .to("log:io.oth.xds?level=INFO")
       }

    def generateOutput(def input) {
        RepositoryConnector repo = new RepositoryConnector()
//        repo.repositoryUrl = repositoryUrl
        repo.repositoryId = repositoryId
        repo.homeCommunityId = homeCommunityId
        def body = input
        def xmlString = XmlUtil.serialize(body)

        SubmitObjectsRequestHelper helper = new SubmitObjectsRequestHelper(repositoryId,homeCommunityId)
        def request = helper.buildFromCDA(getDocumentFromString(xmlString))
        return request
    }

    def procesCda(def input) {

        RepositoryConnector repo = new RepositoryConnector()
        repo.setDefaultUri("https://kihdb-devel.oth.io/services/KIHDocumentRepository")
//        repo.repositoryUrl = repositoryUrl
//        repo.repositoryId = repositoryId
//        repo.homeCommunityId = homeCommunityId
        def body = input
        def xmlString = XmlUtil.serialize(body)
//        def respon = repo.provideAndRegisterCDADocument(xmlString)
        def respon = xdsRepositoryConnector.provideAndRegisterCDADocument(xmlString)
//
        respon
    }

}
