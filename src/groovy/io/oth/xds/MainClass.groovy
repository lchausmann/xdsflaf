package io.oth.xds

import groovy.xml.XmlUtil
import org.net4care.xdsconnector.Utilities.SubmitObjectsRequestHelper
import org.net4care.xdsconnector.service.SubmitObjectsRequest
import org.w3c.dom.Document

import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory

/**
 * Created by lch on 20/06/16.
 */
class MainClass {


    public static void main(String[] args) {
        File directory = new File(args[0])
        for (file in directory.listFiles()) {
            if (file.name.endsWith(".xml")) {
                println "Handling ${file}"
                FileInputStream fs = new FileInputStream(file)
                def slurper = new XmlSlurper().parse(fs)
                def xmlString = XmlUtil.serialize(slurper)

                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance()
                def builder = factory.newDocumentBuilder()
                def document1 = builder.parse(fs)


                SubmitObjectsRequestHelper helper = new SubmitObjectsRequestHelper("2.16.840.1.113883.3.4208.2.5","2.16.840.1.113883.3.4208.3.5")
                def submitObjectsRequest = helper.buildFromCDA(document1)

                println submitObjectsRequest

            }
        }
    }
}
