package io.oth.xds.routes

import io.oth.xds.MainApplication
import org.apache.camel.CamelContext
import org.apache.camel.builder.NotifyBuilder
import org.junit.Test
import org.junit.runner.RunWith
import org.net4care.xdsconnector.ConnectorConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.PropertySource
import org.springframework.test.context.TestPropertySource
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner

import java.util.concurrent.TimeUnit

import static org.junit.Assert.assertTrue

/**
 * Created by lch on 17/06/16.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(MainApplication.class)
@ComponentScan
@EnableAutoConfiguration
@TestPropertySource(locations="classpath:application.properties")
@EnableConfigurationProperties
@PropertySource("classpath:xds.properties")
class FileRouteApplicationTest {
    @Autowired
    private CamelContext camelContext;

//    @Autowired
//    ConnectorConfiguration configuration




    @Test
    public void shouldProduceMessages() throws Exception {

        println "Starting"
        // we expect that one or more messages is automatic done by the Camel
        // route as it uses a timer to trigger
        NotifyBuilder notify = new NotifyBuilder(camelContext).whenDone(1).create();
        assertTrue(notify.matches(40, TimeUnit.SECONDS));
    }
}
